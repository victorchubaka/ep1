#include "../inc/mapa.hpp"
#include "../inc/newgame.hpp"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

Mapa::Mapa(){}
Mapa::~Mapa(){}
void Mapa::gerar_mapa(){
  for (int i=0; i<linhas; i++){
    for (int j=0; j<colunas; j++){
      mapa1[i][j] = '~';
      mapa2[i][j] = '~';
    }
  }
}
void Mapa::imprimir_mapa(){

  cout << "Jogador N.1                                Jogador N.2" << endl;
  cout << "   A  B  C  D  E  F  G  H  I  J  K  L  M | A  B  C  D  E  F  G  H  I  J  K  L  M" << endl;

  for (int i=0; i<linhas; i++){
    if (i<10)
      cout << "0" << i;
    else if (i>=10)
      cout << "" << i;

    for (int j=0; j<colunas; j++){
      cout << " " << mapa1[i][j] << " ";
    }
      cout << "|";
    for (int j=0; j<colunas; j++){
      cout << " " << mapa2[i][j] << " ";
    }
    cout << endl;
  }
  }
void Mapa::clear(){
  system("clear");
}
void Mapa::atualizar_mapa(int p1, int linha, int coluna){
      int i = linha;
      int j = coluna;
      if (p1 == 0){
        mapa1[i][j] = '~';
      }
      else if (p1 == 6){
        mapa1[i][j] = 'P';
      }
      else if (p1 == 5){
        mapa1[i][j] = 'S';
      }
      else if (p1 == 4){
        mapa1[i][j] = 'C';
      }
      else if (p1 == 9){
        mapa1[i][j] = '*';
      }
    }
void Mapa::atualizar_mapa2(int p2, int linha, int coluna){
      int i = linha;
      int j = coluna;
      if (p2 == 0){
        mapa2[i][j] = '~';
      }
      else if (p2 == 6){
        mapa2[i][j] = 'P';
      }
      else if (p2 == 5){
        mapa2[i][j] = 'S';
      }
      else if (p2 == 4){
        mapa2[i][j] = 'C';
      }
      else if (p2 == 9){
        mapa2[i][j] = '*';
      }
    }
