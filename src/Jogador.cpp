#include "../inc/Barcos.hpp"
#include "../inc/Canoa.hpp"
#include "../inc/Submarino.hpp"
#include "../inc/Porta_avioes.hpp"
#include "../inc/mapa.hpp"
#include "../inc/Jogador.hpp"
#include "../inc/newgame.hpp"
#include <iostream>
#include <fstream>
using namespace std;

Jogador::Jogador(){
  Qtdebarcos = 12;
  pontuacao = 0;
}
Jogador::~Jogador(){}

int Jogador::get_Qtdebarcos(){
  return Qtdebarcos;
}
void Jogador::set_Qtdebarcos(int Qtdebarcos){
  this->Qtdebarcos = Qtdebarcos;
}

int Jogador::get_pontuacao(){
  return pontuacao;
}
void Jogador::set_pontuacao(int pontuacao){
  this-> pontuacao = pontuacao;
}

int Jogador::gerar_placar(){
  pontuacao += 1;
  return pontuacao;
}
int Jogador::gerar_barcos(){
  Qtdebarcos -= 1;
  return Qtdebarcos;
}
