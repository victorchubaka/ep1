#include "../inc/Barcos.hpp"
#include "../inc/Canoa.hpp"
#include "../inc/Submarino.hpp"
#include "../inc/Porta_avioes.hpp"
#include "../inc/mapa.hpp"
#include "../inc/newgame.hpp"
#include "../inc/Jogador.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <time.h>
using namespace std;

int main(int argc, char *argv[]){
    srand(time(NULL));
    Newgame * newgame = new Newgame();
    newgame->ler_mapa();
    newgame->gerar_tela();
    while(newgame->determinar_vencedor() == false){
    newgame->atirar1();
    newgame->atirar2();
  }
  delete newgame;
    return 0;
}
