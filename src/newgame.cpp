#include "../inc/Barcos.hpp"
#include "../inc/Canoa.hpp"
#include "../inc/Submarino.hpp"
#include "../inc/Porta_avioes.hpp"
#include "../inc/mapa.hpp"
#include "../inc/newgame.hpp"
#include "../inc/Jogador.hpp"
#include <iostream>
#include <sstream>
#include <time.h>
#include <fstream>
#include <utility>
#include <string>
using namespace std;

Newgame::Newgame(){}
Newgame::~Newgame(){}
//gera a parte grafica no terminal
void Newgame::gerar_tela(){
  atualizar_tela.gerar_mapa();
  atualizar_tela.imprimir_mapa();
}
//interações entre os players
void Newgame::atirar1(){
  Jogador1.set_pontuacao(12 - Jogador2.get_Qtdebarcos());
  int pontuacao = Jogador1.get_pontuacao();
  cout << "Jogador No1. Pontuação atual: "<< pontuacao << endl;
  int i = 0, j = 0;
  cout << "Escolha uma linha (0-13): " << endl;
  cin >> i;
  cout << "Escolha uma coluna(0-13) (use o numero correspondente a letra considerando A=0): " << endl;
  cin >> j;
  if ((i<0 || i>12) || (j<0 || j>12)){
    cout << "Combinação invalida" << endl;
    atirar1();
  }
  atualizar_tela.clear();
  if(local2[i][j] == 4 || local2[i][j] == 5 || local2[i][j] == 6 || local2[i][j] == 9){
    atualizar_tela.imprimir_mapa();
    cout << "Local já bombardeado, atire de novo" << endl;
    atirar1();
  }
  else if(local2[i][j] == 1){
    int num_aleatorio = portaA.anti_missil();
    if (num_aleatorio < 26){
      cout << "Porta-Aviões!! Porta-Aviões!!" << endl;
      local2[i][j] = 6;
      aux_portaA++;
      if(aux_portaA == 4){
        Jogador2.set_Qtdebarcos(12 - (Jogador1.get_pontuacao()+1));
        aux_portaA = 0;
      }
  }
    if (num_aleatorio >= 26){
      cout << "Ataque parado..." << endl;
   }
  }
  else if(local2[i][j] == 2){
    if(aux_submarino[i][j] == 0){
      aux_submarino[i][j] = local2[i][j];
    }
    if (aux_submarino[i][j] == 2){
      aux_submarino[i][j] = aux_submarino[i][j] - 1;
      cout << "Você acertou um Submarino!!" << endl;
  }
    else if (aux_submarino[i][j] == 1){
     cout << "Você destruiu uma parte do Submarino!" << endl;
     local2[i][j] = 5;
     if(local2[i-1][j] == 5 || local2[i+1][j] == 5 || local2[i][j+1] == 5 || local2[i][j-1] == 5){
       Jogador2.set_Qtdebarcos(12 - (Jogador1.get_pontuacao()+1));
     }
     aux_submarino[i][j] = 0;
    }
  }
  else if(local2[i][j] == 3){
    cout << "Espero que esses índios saibam nadar..." << endl;
    local2[i][j] = 4;
    Jogador2.set_Qtdebarcos(12 - (Jogador1.get_pontuacao()+1));
  }
  else if(local2[i][j] == 0){
    local2[i][j] = 9;
    cout << "Splaaaaashh" << endl;
  }
  int embarcacao2 = local2[i][j];
  atualizar_tela.atualizar_mapa2(embarcacao2, i, j);
  atualizar_tela.imprimir_mapa();
}
void Newgame::atirar2(){
  Jogador2.set_pontuacao(12 - Jogador1.get_Qtdebarcos());
  int pontuacao = Jogador2.get_pontuacao();
  cout << "Jogador No2. Pontuação atual: "<< pontuacao << endl;
  int i = 0, j = 0;
  cout << "Escolha uma linha (0-13): " << endl;
  cin >> i;
  cout << "Escolha uma coluna(0-13) (use o numero correspondente a letra considerando A=0): " << endl;
  cin >> j;
  if ((i<0 || i>12) || (j<0 || j>12)){
    cout << "Combinação invalida" << endl;
    atirar2();
  }
  atualizar_tela.clear();
  if(local1[i][j] == 4 || local1[i][j] == 5 || local1[i][j] == 6 || local1[i][j] == 9){
    atualizar_tela.imprimir_mapa();
    cout << "Local já bombardeado, atire de novo" << endl;
    atirar1();
  }
  else if(local1[i][j] == 1){
    int num_aleatorio = portaA.anti_missil();
    if (num_aleatorio < 26){
      cout << "Porta-Aviões!! Porta-Aviões!!" << endl;
      local1[i][j] = 6;
      aux_portaA++;
      if(aux_portaA == 4){
        Jogador2.set_Qtdebarcos(12 - (Jogador1.get_pontuacao()+1));
        aux_portaA = 0;
      }
  }
    if (num_aleatorio >= 26){
      cout << "Ataque parado..." << endl;
   }
  }
  else if(local1[i][j] == 2){
    if(aux_submarino[i][j] == 0){
      aux_submarino[i][j] = local1[i][j];
    }
    if (aux_submarino[i][j] == 2){
      aux_submarino[i][j] = aux_submarino[i][j] - 1;
      cout << "Você acertou um Submarino!!" << endl;
  }
    else if (aux_submarino[i][j] == 1){
     cout << "Você destruiu uma parte do Submarino!" << endl;
     local1[i][j] = 5;
     if(local1[i-1][j] == 5 || local1[i+1][j] == 5 || local1[i][j+1] == 5 || local1[i][j-1] == 5){
       Jogador1.set_Qtdebarcos(12 - (Jogador1.get_pontuacao()+1));
     }
     aux_submarino[i][j] = 0;
    }
  }
  else if(local1[i][j] == 3){
    cout << "Espero que esses índios saibam nadar..." << endl;
    local1[i][j] = 4;
    Jogador1.set_Qtdebarcos(12 - (Jogador1.get_pontuacao()+1));
  }
  else if(local1[i][j] == 0){
    local1[i][j] = 9;
    cout << "Splaaaaashh" << endl;
  }
  int embarcacao1 = local1[i][j];
  atualizar_tela.atualizar_mapa(embarcacao1, i, j);
  atualizar_tela.imprimir_mapa();
}
bool Newgame::determinar_vencedor(){
  bool Jogador1_vencedor = false;
  bool Jogador2_vencedor = false;
  if (Jogador1.get_pontuacao() == 12){
    Jogador1_vencedor = true;
    cout << "O jogador No.1 Saiu vitorioso, mais sorte da próxima vez frangote!" << endl;
    return Jogador1_vencedor;
  }
  else if (Jogador2.get_pontuacao() == 12){
    Jogador2_vencedor = true;
    cout << "O jogador No.2 Saiu vitorioso, mais sorte da próxima vez frangote!" << endl;
    return Jogador2_vencedor;
  }
  return false;
}

//lê arquivo txt
void Newgame::ler_mapa(){
  int aux = -1;
  ifstream leitura_mapa;
  leitura_mapa.open("doc/map_1.txt");
  if (leitura_mapa.is_open()){
    while (!leitura_mapa.eof()){
      string nome1, sentido1, largura, altura;
      int linha, coluna;
      getline(leitura_mapa, largura);
      if (largura[0]=='#' || largura==""){
        continue;
      }
      istringstream linha_arquivo {
        largura
      };
      linha_arquivo >> largura;
      linha_arquivo >> altura;
      linha = stoi(largura);
      coluna = stoi(altura);
      linha_arquivo >> nome1;
      linha_arquivo >> sentido1;
      aux++;
      if(nome1 == "canoa"){
        colocar_Canoa(linha, coluna, aux);
      }
      else if(nome1 == "submarino"){
        colocar_Submarino(linha, coluna, aux, sentido1);
      }
      else if(nome1 == "porta-avioes"){
        colocar_Pavioes(linha, coluna, aux, sentido1);
      }
    }
  }
  leitura_mapa.close();
}

void Newgame::colocar_Canoa(int i, int j, int contador){
  if (contador<6){
    local1[i][j] = 3;
  }
  else if (contador>=6){
    local2[i][j] = 3;
  }
}
void Newgame::colocar_Submarino(int i, int j, int contador, string sentido1){
  if(contador>5 && contador<10){
    local1[i][j] = 2;
  if (sentido1 == "cima"){
    local1[i-1][j] = 2;
  }
  if (sentido1 == "baixo"){
    local1[i+1][j] = 2;
  }
  if (sentido1 == "esquerda"){
    local1[i][j-1] = 2;
  }
  if (sentido1 == "direita"){
    local1[i][j+1] = 2;
  }
}
else if(contador>=10){
      local2[i][j] = 2;
    if (sentido1 == "cima"){
      local2[i+1][j] = 2;
}
    if (sentido1 == "baixo"){
      local2[i-1][j] = 2;
}
    if (sentido1 == "esquerda"){
      local2[i][j-1] = 2;
    }
    if (sentido1 == "direita"){
      local2[i][j+1] = 2;
    }
    }
}
void Newgame::colocar_Pavioes(int i, int j, int contador, string sentido1){
  if(contador>9 && contador<12){
    local1[i][j] = 1;
    if (sentido1 == "cima"){
      local1[i-1][j] = 1;
      local1[i-2][j] = 1;
      local1[i-3][j] = 1;
    }
    if (sentido1 == "baixo"){
      local1[i+1][j] = 1;
      local1[i+2][j] = 1;
      local1[i+3][j] = 1;
    }
    if (sentido1 == "esquerda"){
      local1[i][j-1] = 1;
      local1[i][j-2] = 1;
      local1[i][j-3] = 1;
    }
    if (sentido1 == "direita"){
      local1[i][j+1] = 1;
      local1[i][j+2] = 1;
      local1[i][j+3] = 1;
    }
}
  else if(contador>=12){
    local2[i][j] = 1;
    if (sentido1 == "cima"){
      local2[i-1][j] = 1;
      local2[i-2][j] = 1;
      local2[i-3][j] = 1;
    }
    if (sentido1 == "baixo"){
      local2[i+1][j] = 1;
      local2[i+2][j] = 1;
      local2[i+3][j] = 1;
    }
    if (sentido1 == "esquerda"){
      local2[i][j-1] = 1;
      local2[i][j-2] = 1;
      local2[i][j-3] = 1;
    }
    if (sentido1 == "direita"){
      local2[i][j+1] = 1;
      local2[i][j+2] = 1;
      local2[i][j+3] = 1;
    }
  }
}
