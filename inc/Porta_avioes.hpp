#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include "Barcos.hpp"
#include <string>
#include <time.h>
using namespace std;

class Porta_Avioes:public Barcos{
    private:
        int especial;

    public:
        Porta_Avioes();
        ~Porta_Avioes();
        int anti_missil();
        int get_tamanho();
        void set_tamanho(int tamanho);
};
#endif
