#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <string>
#include "Barcos.hpp"
using namespace std;

class Submarino:public Barcos{
    public:
        Submarino();
        ~Submarino();
        int get_tamanho();
        void set_tamanho(int tamanho);
};

#endif
