#ifndef MAPA_HPP
#define MAPA_HPP

#include "Barcos.hpp"
#include <string>
#include <vector>
using namespace std;

class Mapa{
    private:
        const static int colunas=13;
        const static int linhas=13;
        char mapa1[linhas][colunas];
        char mapa2[linhas][colunas];

    public:
        Mapa();
        ~Mapa();
        void gerar_mapa();
        void imprimir_mapa();
        void clear();
        void atualizar_mapa(int, int, int);
        void atualizar_mapa2(int, int, int);
};
#endif
