#ifndef NEWGAME_HPP
#define NEWGAME_HPP

#include "Jogador.hpp"
#include "Barcos.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "Porta_avioes.hpp"
#include "mapa.hpp"
#include <string>
#include <vector>
#include <time.h>
#include <utility>
using namespace std;

class Newgame{
  private:
    const static int colunas=13;
    const static int linhas=13;
    string mapa_atualizado[linhas][colunas];
    int local1[linhas][colunas] = {0};
    int local2[linhas][colunas] = {0};
    int aux_submarino[linhas][colunas];
    int aux_portaA;
    Mapa atualizar_tela;
    Porta_Avioes portaA;
    Submarino Sub;
    Canoa Can;
    Jogador Jogador1;
    Jogador Jogador2;

  public:
    Newgame();
    ~Newgame();
    void ler_mapa();
    void gerar_tela();
    void atirar1();
    void atirar2();
    bool determinar_vencedor();

    void colocar_Canoa(int , int , int);
    void colocar_Submarino(int , int , int , string);
    void colocar_Pavioes(int , int , int, string);
};

#endif
