#ifndef CANOA_HPP
#define CANOA_HPP

#include <string>
#include "Barcos.hpp"
using namespace std;

class Canoa:public Barcos{
    public:
      Canoa();
      ~Canoa();
      int get_tamanho();
      void set_tamanho(int tamanho);
};

#endif
