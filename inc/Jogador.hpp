#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include "Barcos.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "Porta_avioes.hpp"
#include "mapa.hpp"
#include <string>
#include <vector>
using namespace std;

class Jogador{
  private:
    int Qtdebarcos;
    int pontuacao;

  public:
    Jogador();
    ~Jogador();
    int get_Qtdebarcos();
    void set_Qtdebarcos(int Qtdebarcos);
    int get_pontuacao();
    void set_pontuacao(int pontuacao);
    int gerar_placar();
    int gerar_barcos();
};

#endif
