# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval)

## Instruções

&nbsp;&nbsp;Recomenda-se construir o corpo do README com no mínimo no seguinte formato:

* Descrição do projeto
* Instruções de execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

## Observações

* Erro ao tentar ler os outros mapas pré-programados, caso queira usá-los, copiar seu conteúdo dentro do map_1.txt
* 
